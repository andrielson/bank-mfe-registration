import { Component } from '@angular/core';

@Component({
  selector: 'bank-mfe-registration-root',
  template: `<router-outlet></router-outlet>`,
})
export class AppComponent {
  title = 'bank-mfe-registration';
}
