import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { EmptyRouteComponent } from "./empty-route/empty-route.component";

const routes: Routes = [
  {
    path: "registration",
    children: [
      {
        path: "welcome",
        loadChildren: () =>
          import("./welcome/welcome.module").then((m) => m.WelcomeModule),
      },
      {
        path: "",
        pathMatch: "full",
        redirectTo: "welcome",
      },
    ],
  },
  {
    path: "**",
    component: EmptyRouteComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
